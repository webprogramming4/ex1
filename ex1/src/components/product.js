import './product.css'

function Product(props) {
  return (
    <div className="product-item">
    <div className="product-desc">
      <span className="product-desc-title">Product type:</span>
      <div className="product-name item-desc"> {props.name}</div>
      <span className="product-desc-title">Price:</span>
      <div className="product-price item-desc">{props.price}</div>
    </div>
    <div className="product-image">
    <img src={props.image} className="clothing-image" />
      </div>
    </div>
  );
}

export default Product;
