import "./App.css";
import Product from "./components/product";

function App() {
  const products = [
    { name: "blue dress", price: 190, imageURL: "./images/dress.jpg" },
    {
      name: "shoes",
      price: 300,
      imageURL: "./images/shoes.jpg",
    },
    { name: "Bag", price: 200, imageURL: "./images/bag.jpg" },
    { name: "t-shirt", price: 50, imageURL: "./images/tshirt.jpeg" },
    { name: "socks", price: 30, imageURL: "./images/socks.jpg" },
  ];
  const productsShow = products.map((p) => (
    <Product
      key={p.name}
      name={p.name}
      price={p.price}
      image={p.imageURL}
    ></Product>
  ));
  return (
    <div className="products">
      <div className="header">Product list</div>
      <div> {productsShow}</div>
    </div>
  );
}

export default App;
